---
title: "Palau"
date: 2022-10-09T10:58:18-03:00
draft: false
capital: "Ngerulmud"
bandeira: https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Flag_of_Palau.svg/383px-Flag_of_Palau.svg.png
pop: "18 092 habitantes"
area: "458 km²"
dens: "39,50 habitantes/km²"
lingua: "Palauense e Inglês"
moeda: "Dólar americano"
---

