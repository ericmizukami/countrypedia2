---
title: "Papua-Nova Guiné"
date: 2022-10-09T10:58:51-03:00
draft: false
capital: "Port Moresby"
bandeira: https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/Flag_of_Papua_New_Guinea.svg/340px-Flag_of_Papua_New_Guinea.svg.png
pop: "8,947 milhões habitantes"
area: "462 840 km²"
dens: "19,33 habitantes/km²"
lingua: "Tok Pisin, Inglês e Hiri Motu"
moeda: "Kina"
---

