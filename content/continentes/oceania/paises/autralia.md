---
title: "Autrália"
date: 2022-10-09T10:58:12-03:00
draft: false
capital: "Camberra"
bandeira: https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Flag_of_Australia.svg/383px-Flag_of_Australia.svg.png
pop: "25,69 milhões habitantes"
area: "7,688 milhões km²"
dens: "3,34 habitantes/km²"
lingua: "Inglês australiano"
moeda: "Dólar australiano"
---

