---
title: "Angola"
date: 2022-10-09T10:55:02-03:00
draft: false
capital: "Luanda"
bandeira: https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Flag_of_Angola.svg/383px-Flag_of_Angola.svg.png
pop: "32,87 milhões habitantes"
area: "1,247 milhões km²"
dens: "26,36 habitantes/km²"
lingua: "Português"
moeda: "Kwanza"
---

