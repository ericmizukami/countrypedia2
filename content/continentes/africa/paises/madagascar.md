---
title: "Madagascar"
date: 2022-10-09T10:55:51-03:00
draft: false
capital: "Antananarivo"
bandeira: https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Flag_of_Madagascar.svg/383px-Flag_of_Madagascar.svg.png
pop: "27,69 milhões habitantes"
area: "587 041 km²"
dens: "47,17 habitantes/km²"
lingua: "Malgaxe e Francês"
moeda: "Ariary"
---

