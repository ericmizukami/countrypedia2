---
title: "China"
date: 2022-10-09T10:56:25-03:00
draft: false
capital: "Beijing"
bandeira: https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Flag_of_the_People%27s_Republic_of_China.svg/383px-Flag_of_the_People%27s_Republic_of_China.svg.png
pop: "1,402 bilhões habitantes"
area: "9,597 milhões km²"
dens: "146,09 habitantes/km²"
lingua: "Mandarim"
moeda: "Renminbi"
---

