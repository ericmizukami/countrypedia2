---
title: "Índia"
date: 2022-10-09T10:56:30-03:00
draft: false
capital: "Nova Déli"
bandeira: https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Flag_of_India.svg/420px-Flag_of_India.svg.png
pop: "1,38 bilhões habitantes"
area: "3,287 milhões km²"
dens: "419,84 habitantes/km²"
lingua: "Hindi e Inglês"
moeda: "Rupia"
---

