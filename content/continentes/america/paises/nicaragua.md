---
title: "Nicarágua"
date: 2022-09-28T15:56:42-03:00
draft: false
capital: "Manágua"
bandeira: https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Flag_of_Nicaragua.svg/383px-Flag_of_Nicaragua.svg.png
pop: "6,625 milhões habitantes"
area: "130 373 km²"
dens: "50,81 habitantes/km²"
lingua: "Espanhol"
moeda: "Córdoba nicaraguense"
---