---
title: "Canadá"
date: 2022-10-09T10:54:31-03:00
draft: false
capital: "Ottawa"
bandeira: https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Flag_of_Canada_%28Pantone%29.svg/458px-Flag_of_Canada_%28Pantone%29.svg.png
pop: "38,01 milhões habitantes"
area: "9,985 milhões km²"
dens: "3,81 habitantes/km²"
lingua: "Inglês e Francês"
moeda: "Dólar canadense"
---

