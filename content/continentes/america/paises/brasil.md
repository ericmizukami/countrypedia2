---
title: "Brasil"
date: 2022-09-22T21:06:36-03:00
draft: false
capital: "Brasília"
bandeira: https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Flag_of_Brazil.svg/413px-Flag_of_Brazil.svg.png
pop: "212,6 milhões habitantes"
area: "8,516 milhões km²"
dens: "24,96 habitantes/km²"
lingua: "Português"
moeda: "Real"
---







