---
title: "Alemanha"
date: 2022-10-09T10:57:44-03:00
draft: false
capital: "Berlim"
bandeira: https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Flag_of_Germany.svg/383px-Flag_of_Germany.svg.png
pop: "93,24 milhões habitantes"
area: "357 588 km²"
dens: "260,75 habitantes/km²"
lingua: "Alemão"
moeda: "Euro"
---

